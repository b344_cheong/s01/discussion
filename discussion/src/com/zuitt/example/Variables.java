package com.zuitt.example;
// A "package" in Java is used to group related classes. Think of it as a folder in a file directory.
    // Packages are divided into two categories:
        // 1. Built - in Packages (Packages from the Java API)
        // 2. User-defined Packages (Create your own packages)

// Package creation in Java follows the "reverse domain name notation" for the naming convention.

public class Variables {
    public static void main (String[] args){
        // Naming Convention
            // The terminology used for variable name is identifier.
            // All identifiers should begin with a letter (A to Z, a to z), currency($) or underscore(_)
            // After the first character, identifiers can have any combination of characters.
            // Most importantly, identifiers are case-sensitive.

        // Variable declaration:
        int age;
        char middleName;

        // Variable declaration together with initialization:
        int x;
        int y = 0;

        // Initialization after declaration:
        x = 2;

        // Output to the system:
        System.out.println("The value of y is " + y + "and the value of x is " + x);

        // Primitive Data Types:
            // Predefined within the Java Programming Language which is used for single-valued variables with limited capabilities.

        // int: whole number values
        int wholeNumber = 100;
        System.out.println("The value of int is: " + wholeNumber);

        // Long: L is added to the end of the long number to be recognized:
        long worldPopulation = 47821468712476L;
        System.out.println("The value of long is: " + worldPopulation);

        // Floating Value: f is added to the end of the floating value, up to 7 decimal places
        float piFloat = 3.141592653559f;
        System.out.println("The value of piFloat is: " + piFloat);

        // Double - Floating values
        double piDouble = 3.141592653559;
        System.out.println("The value of piDouble is: " + piDouble);

        // char - single character
        // uses single quote
        // char uses '' while string uses ""
        char letter = 'a';
        System.out.println(letter);

        // boolean - true or false value (1 or 0)
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println("Is Mary in Love? It's "+ isLove + ".");
        System.out.println("Is Jane taken? It's " + isTaken + ".");

        // Constants
        // Java uses the "final" keyword so that the variable's value cannot be changed;

        final int PRINCIPAL = 3000;
        System.out.println("The Principal Position is worth " + PRINCIPAL + " men!");

        // Non-primitive Data
            // Also known as reference data types refers to instances or objects
            // Does not directly store the value of a variable, but rather remembers the reference to that variable
        // String
        // Stores a sequence or array of characters
        // String are actually objects that can use methods

        String username = "JSmith";
        System.out.println("My username is " + username + "!");

        // Sample string method
        int stringLength = username.length();
        System.out.println("My username has " + stringLength + " characters!");
    }
}
